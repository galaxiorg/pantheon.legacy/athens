--[[
	athens | Package manager for Pantheon
	       | Pantheon Project (galaxiorg/pantheon)

	Filename
	  main.lua
	Author
	  daelvn (https://gitlab.com/daelvn)
	Version
	  athens-i0
	License
	  MIT License (LICENSE.md)
	Documentation
	  manual* athens:about
		* Pantheon/manual : Manual viewer
	Description
	  Athens is a package manager for Pantheon that supports dependencies, custom
		sources, and a set of various options.
	Dependencies
		Pantheon/libini *
		Pantheon/libgdmp *
		Pantheon/libhttp *
]]--
